#ifndef _HARDWARE_CONFIG_H
#define _HARDWARE_CONFIG_H

#define CONFIG_VOLTAGE_ADC_ADDR 0x68
#define CONFIG_CURRENT_ADC_ADDR 0x69

//TODO correct addresses
#define DAC_ADDR 0x60

#define OLED_ADDR SSD1306_I2C_ADDR_0

#define OLED_WIDTH 128
#define OLED_HEIGHT 64

#define I2C_BUS 0
#define SCL_PIN 5
#define SDA_PIN 4

#define ENCODER0_PIN0 14
#define ENCODER0_PIN1 16

#define ENCODER1_PIN0 13
#define ENCODER1_PIN1 12

#define BUTTON0_PIN 2
#define BUTTON1_PIN 0

#define VOLTAGE_ADC_TO_FLOAT(adc) ( (adc) * (float) 0.005 )
#define CURRENT_ADC_TO_FLOAT(adc) ( (adc) * (float) 0.0005 )

// >>5 because adc raw values are 18 bit and calculated LSBs are for 12 bit
// 18-12-1 = 5 (-1 because 3421s return signed values)
#define VOLTAGE_ADC_RAW_TO_FLOAT(raw) ( VOLTAGE_ADC_TO_FLOAT((raw>>5)) )
#define CURRENT_ADC_RAW_TO_FLOAT(raw) ( CURRENT_ADC_TO_FLOAT((raw>>5)) )

#define DAC_CHANNEL_I_SET 0
#define DAC_CHANNEL_V_SET 1
#define DAC_CHANNEL_V_SET_DCDC 2

#define DAC_STEP 0.0005

#define R1 2000.0
#define R3 24000.0
#define V_REF_DCDC 0.8f

#define VOLTAGE_TARGET_DCDC_TO_DAC_VOLTAGE(required) ( (R1/R3)*(V_REF_DCDC*(1 + 2 *(R3/R1)) - (required) ) )

#define DAC_VOLTAGE_TO_RAW(v) ( (int)((v)/DAC_STEP) )

#define VOLTAGE_DAC_FLOAT_TO_RAW(f) ( DAC_VOLTAGE_TO_RAW( (f)/10 ) )
#define CURRENT_DAC_FLOAT_TO_RAW(f) ( DAC_VOLTAGE_TO_RAW( (f) ) )

#endif