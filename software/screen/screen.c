

#include <stdio.h>

#include <i2c/i2c.h>

#include <fonts/fonts.h>
#include <ssd1306/ssd1306.h>

#include "var_store/var_store.h"

#include "hardware_config.h"

#define FONT_NO 7 //5, 6, 7, 8 works fine

#define SCREEN_FPS 10

static const ssd1306_t dev = {
	.protocol = SSD1306_PROTO_I2C,
	.i2c_dev.bus      = I2C_BUS,
	.i2c_dev.addr     = OLED_ADDR,
	.width    = OLED_WIDTH,
	.height   = OLED_HEIGHT
};

static uint8_t bit_reverse(uint8_t b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}

static void frame_buffer_flip(uint8_t* frame_buffer, int width, int height){
	uint8_t first, last;
	int i;
	const BUFF_SIZE = width * height / 8;
	for(i = 0; i< BUFF_SIZE / 2; i++){
		first = bit_reverse(frame_buffer[i]);
		last = bit_reverse(frame_buffer[BUFF_SIZE-i]);
		frame_buffer[i] = last;
		frame_buffer[BUFF_SIZE-i] = first;
	}
}

static uint8_t frame_buffer[OLED_WIDTH * OLED_HEIGHT / 8];

static void ssd1306_task(void *pvParameters)
{

	while (ssd1306_init(&dev) != 0) {
		printf("%s: failed to init SSD1306 lcd\n", __func__);
		vTaskDelay(pdMS_TO_TICKS(1000));
	}
	ssd1306_set_whole_display_lighting(&dev, true);
	vTaskDelay(pdMS_TO_TICKS(500));

	printf("%s: Started user interface task\n", __FUNCTION__);
	vTaskDelay(pdMS_TO_TICKS(500));

	ssd1306_set_whole_display_lighting(&dev, false);

	char text[20];
	float v, v_set, i, i_set;
	while (1) {
		v = var_get(VAR_V);
		i = var_get(VAR_I);
		v_set = var_get(VAR_V_SET);
		i_set = var_get(VAR_I_SET);

		ssd1306_fill_rectangle(&dev, frame_buffer, 0, 0, OLED_WIDTH, OLED_HEIGHT, OLED_COLOR_BLACK);
		ssd1306_fill_rectangle(&dev, frame_buffer, 0, 0, 10, 10, OLED_COLOR_WHITE);
		sprintf(text, "V:     %6.3f V", v);
		ssd1306_draw_string(&dev, frame_buffer,font_builtin_fonts[FONT_NO], 0, 0, text, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
		sprintf(text, "V_SET: %6.3f V", v_set);
		ssd1306_draw_string(&dev, frame_buffer,font_builtin_fonts[FONT_NO], 0, 15, text, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
		sprintf(text, "I:     %6.4f A", i);
		ssd1306_draw_string(&dev, frame_buffer,font_builtin_fonts[FONT_NO], 0, 35, text, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
		sprintf(text, "I_SET: %6.4f A", i_set);
		ssd1306_draw_string(&dev, frame_buffer, font_builtin_fonts[FONT_NO], 0, 50, text, OLED_COLOR_WHITE, OLED_COLOR_BLACK);


		frame_buffer_flip(frame_buffer, OLED_WIDTH, OLED_HEIGHT);
		if (ssd1306_load_frame_buffer(&dev, frame_buffer)) {
			printf("frame skipped\n");
		}

		vTaskDelay(pdMS_TO_TICKS(1000/SCREEN_FPS));
	}
}

void screen_init(){
	xTaskCreate(ssd1306_task, "ssd1306_task", 512, NULL, 2, NULL);
}
