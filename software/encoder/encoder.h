#ifndef _ENCODER_H
#define _ENCODER_H

typedef struct {
    int gpio[2];
    float mean_changes;
    int counter, changes, last;
}encoder;

void encoder_init(encoder* enc, int gpio1, int gpio2);

int encoder_get_delta(encoder* enc);

#endif