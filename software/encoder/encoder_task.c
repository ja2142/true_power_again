
#include <stdlib.h>

#include "hardware_config.h"
#include "encoder.h"
#include "var_store/var_store.h"

static void encoder_task(){
    float v_set, i_set;
    encoder encoders[2];
    encoder_init(&encoders[0], ENCODER0_PIN0, ENCODER0_PIN1 );
    encoder_init(&encoders[1], ENCODER1_PIN0, ENCODER1_PIN1 );
    while(1){
		vTaskDelay(1);
        i_set = var_get(VAR_I_SET);		
        v_set = var_get(VAR_V_SET);	

        i_set += CURRENT_ADC_TO_FLOAT( encoder_get_delta(&encoders[0]) );
        v_set += VOLTAGE_ADC_TO_FLOAT( encoder_get_delta(&encoders[1]) );
		
        var_set(VAR_I_SET, i_set);		
        var_set(VAR_V_SET, v_set);
	}

}

void start_encoder_task(){
    xTaskCreate(encoder_task, "encoder_task", 256, NULL, 2, NULL);
}
