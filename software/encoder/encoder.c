#include "encoder.h"
#include <stdlib.h>

#include "esp8266.h"

static const char gray_to_bin[4] = {0, 1, 3, 2};

static int changesToAcceleration(float x){
	x = abs(x);
	if(x < 5) return 1;
	return 1+x*x/3-25/3;
}

void encoder_init(encoder* enc, int gpio0, int gpio1){
    int i;
    
    memset(enc, 0, sizeof(encoder) );
    enc->gpio[0] = gpio0;
    enc->gpio[1] = gpio1;

    for (i = 0; i < 2; i++) {
        //gpio_set_pullup(enc->gpio[i], 1, 0); //using hardware pullups anyway
	    gpio_enable(enc->gpio[i], GPIO_INPUT);
    }
}

int encoder_get_delta(encoder* enc){
	unsigned int new = gpio_read(enc->gpio[0])?1:0;
	new |= gpio_read(enc->gpio[1])?2:0;
	unsigned int diff = 0;
    if(enc->last == 0 && new == 1){
		enc->changes += 4;
        diff = 1;
	} else if(enc->last == 1 && new == 0){
		diff = -1;
		enc->changes -= 4;
	}
	if(enc->counter++ >= 100){
		enc->counter = 0;
		enc->mean_changes = (enc->mean_changes + enc->changes)/2; 
		enc->changes = 0;
	}
	
	enc->last = new;
	return diff * changesToAcceleration(enc->mean_changes);
}
