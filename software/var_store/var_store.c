#include "var_store.h"

static float vars[VAR_LAST];

#define I_SET_MAX 2.048f
#define V_SET_MAX 20.48f

float normalize(float f, var_store_variable v){
    switch (v)
    {
        case VAR_V:
        case VAR_I:
			return f>0? f:0; 

		case VAR_I_SET:
			if(f<0){
				return 0;
			}else if(f>I_SET_MAX){
				return I_SET_MAX;
			} else return f;

		case VAR_V_SET:
			if(f<0){
				return 0;
			}else if(f>V_SET_MAX){
				return V_SET_MAX;
			} else return f;

        default:
            return f;
    }
}

void var_set(var_store_variable v, float val){
    vars[v] = normalize(val, v);
}

float var_get(var_store_variable v){
    return vars[v];
}
