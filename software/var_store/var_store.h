#ifndef _VAR_STORE_H
#define _VAR_STORE_H

typedef enum{
    VAR_V,
    VAR_V_SET,
    VAR_I,
    VAR_I_SET,

    VAR_LAST
}var_store_variable;

void var_set(var_store_variable v, float val);
float var_get(var_store_variable v);

#endif
