#ifndef _MCP4728_H
#define _MCP4728_H

#include <stdint.h>

void dac_write(const uint8_t bus, const uint8_t addr, int channel, uint16_t val);

#endif