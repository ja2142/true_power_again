#include <stdint.h>
#include <stdlib.h>

#define MCP4728_COMM_MULTI_WRITE 0x40 
#define MCP4728_COMM_SINGLE_WRITE 0x51

#define MCP4728_V_REF_INTERNAL 0x80 

void dac_write(const uint8_t bus, const uint8_t addr, int channel, uint16_t val){
    uint8_t to_write[3];
    memset(to_write, 0, sizeof(to_write));
    //bit 0 = !UDAC = 0 - update immediately
    to_write[0] = MCP4728_COMM_SINGLE_WRITE | (channel<<1 );
    //config and upper 4 bits
    to_write[1] = MCP4728_V_REF_INTERNAL | ((val & 0xF00)>>8);
    to_write[2] = val & 0xFF;

    i2c_slave_write(bus, addr, NULL, to_write, sizeof(to_write));
}
