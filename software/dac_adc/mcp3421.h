#ifndef _MCP3421_H
#define _MCP3421_H

#include <stdint.h>

int adc_init(const uint8_t bus, const uint8_t addr);

int adc_read(const uint8_t bus, const uint8_t addr);

#endif