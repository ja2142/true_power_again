#include "mcp3421.h"

#include <stdio.h>

#include <i2c/i2c.h>

static void print_hex_str(const uint8_t* str, const size_t length) {
	int i;
		for (i=0; i<length; i++) {
		printf("%02x,", str[i]);
	}
}

int adc_init(const uint8_t bus, const uint8_t addr){
	// 0 00  1      11              00
	//              ^3SPS(18bit)    ^PGA gain *1
	//       ^ continous conversion
	uint8_t config_byte = 0x1C;
	i2c_slave_write(bus, addr, NULL, &config_byte, 1);
	return 0;
}

int adc_read(const uint8_t bus, const uint8_t addr){
	uint8_t data[4];
	i2c_slave_read(bus, addr, NULL, data, sizeof(data));
	
	int val = data[2] | data[1]<<8 | (data[0] & 0x1)<<16;
		if(data[0] & 0x2){ // returned value is negative which shouldnt happen
		val = 0;
	}
	return val;
}