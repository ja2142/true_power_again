
#include <stdlib.h>

#include <FreeRTOS.h>

#include "var_store/var_store.h"
#include "hardware_config.h"

#define VOLTAGE_OFFSET 0.5

static void da_ad_task(void* arg){
	int v,i;
	float v_set, i_set, v_set_dcdc;
	adc_init(I2C_BUS, CONFIG_CURRENT_ADC_ADDR);
 	adc_init(I2C_BUS, CONFIG_VOLTAGE_ADC_ADDR);
	while(1){
		// adcs
		v = adc_read(I2C_BUS, CONFIG_VOLTAGE_ADC_ADDR);
		i = adc_read(I2C_BUS, CONFIG_CURRENT_ADC_ADDR);
		var_set(VAR_V, VOLTAGE_ADC_RAW_TO_FLOAT(v));
		var_set(VAR_I, CURRENT_ADC_RAW_TO_FLOAT(i));

		// dac
		v_set = var_get(VAR_V_SET);
		i_set = var_get(VAR_I_SET);

		dac_write(I2C_BUS, DAC_ADDR, DAC_CHANNEL_I_SET, CURRENT_DAC_FLOAT_TO_RAW(i_set));
		dac_write(I2C_BUS, DAC_ADDR, DAC_CHANNEL_V_SET, VOLTAGE_DAC_FLOAT_TO_RAW(v_set));

		v_set_dcdc = v_set + VOLTAGE_OFFSET;
		v_set_dcdc = VOLTAGE_TARGET_DCDC_TO_DAC_VOLTAGE(v_set_dcdc);

		dac_write(I2C_BUS, DAC_ADDR, DAC_CHANNEL_V_SET_DCDC, DAC_VOLTAGE_TO_RAW(v_set_dcdc));

		vTaskDelay(pdMS_TO_TICKS(400));
	}	
}

void start_adc_dac_task(){
    xTaskCreate(da_ad_task, "da_ad_task", 256, NULL, 4, NULL);
}
